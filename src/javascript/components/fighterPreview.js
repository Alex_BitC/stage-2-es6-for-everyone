import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const nameElement = createElement({
    tagName: 'div',
    className: `arena___fighter-name`,
  });
  nameElement.innerHTML =   `${fighter.name}`;

  const infoElement = createElement({
    tagName: 'div',
    className: `arena___fighter-name`,
  });
  infoElement.innerHTML =   `
    <div>Health: ${fighter.health}</div>
    <div>Attack: ${fighter.attack}</div>
    <div>Defense: ${fighter.defense}</div>
  `;
  const imageElement = createFighterImage(fighter);
  fighterElement.append(nameElement, infoElement, imageElement);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
