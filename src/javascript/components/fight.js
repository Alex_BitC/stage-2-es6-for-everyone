import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  return getHitPower(attacker) - getBlockPower(defender) > 0 ? getHitPower(attacker) - getBlockPower(defender) : 0;
  // return damage
}

export function getHitPower(fighter) {
  const minCriticalHitChance = 1;
  const maxCriticalHitChance = 2;
  const criticalHitChance = getRandomFrom(minCriticalHitChance, maxCriticalHitChance);
  return fighter.attack * criticalHitChance;
  // return hit power
}

export function getBlockPower(fighter) {
  const minDodgeChance = 1;
  const maxDodgeChance = 2;
  const dodgeChance = getRandomFrom(minDodgeChance, maxDodgeChance);
  return fighter.defense * dodgeChance;
  // return block power
}

function getRandomFrom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
